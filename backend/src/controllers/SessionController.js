const connnection = require('../database/connection')

module.exports = {
    async create(request, response) {
        const { id } = request.body;

        console.log(id);

        const ong = await connnection('ongs')
            .where('id', id)
            .select('name')
            .first();

        if (!ong) {
            return response.status(400).json({ error: "Ops esse ID não existe" })
        }
        return response.json(ong)
    }
}