const generateUniqueId = require('../utils/generationUniqueId')
const connnection = require('../database/connection')
const crypto = require('crypto')


module.exports = {

    async index(request, response) {
        const ongs = await connnection('ongs').select('*')
        return response.json(ongs)

    },

    async create(request, response) {
        const { name, email, whatsapp, city, uf } = request.body;

        const id = generateUniqueId();
        await connnection('ongs').insert({
            id,
            name,
            email,
            whatsapp,
            city,
            uf,

        })


        return response.json({ id })
    }
}