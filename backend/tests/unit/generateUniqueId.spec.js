const generateUniqueId = require('../../src/utils/generationUniqueId')

describe('Generate Unique ID', () =>{
    it('shoud generate unique ID', ()=>{
        const id = generateUniqueId();
                
        expect(id).toHaveLength(8);
    })
})